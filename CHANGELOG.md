# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.0.16](https://gitlab.com/tjmonsi/lit-router/compare/v0.0.15...v0.0.16) (2020-10-14)


### Bug Fixes

* fix changeUrl ([4a14d37](https://gitlab.com/tjmonsi/lit-router/commit/4a14d377953a0c18013939083b0f81e459958668))

### [0.0.15](https://gitlab.com/tjmonsi/lit-router/compare/v0.0.14...v0.0.15) (2020-09-28)


### Bug Fixes

* fix some bugs ([16c9748](https://gitlab.com/tjmonsi/lit-router/commit/16c9748fad6bb42ff79e891f0e8ef38246337cf3))

### [0.0.14](https://gitlab.com/tjmonsi/lit-router/compare/v0.0.13...v0.0.14) (2020-08-31)


### Bug Fixes

* update typing return of preload ([5aaff6e](https://gitlab.com/tjmonsi/lit-router/commit/5aaff6ef4957f02dc9822b4c447e010a9548a214))

### [0.0.13](https://gitlab.com/tjmonsi/lit-router/compare/v0.0.12...v0.0.13) (2020-08-31)


### Features

* add event handler and change url ([df0cb1c](https://gitlab.com/tjmonsi/lit-router/commit/df0cb1cb36605928b93955708ef4b917b9c6570b))

### [0.0.12](https://gitlab.com/tjmonsi/lit-router/compare/v0.0.11...v0.0.12) (2020-08-31)


### Features

* add preload and fallback ([4377e57](https://gitlab.com/tjmonsi/lit-router/commit/4377e570c4e52eca050ddb61f1cbf695fde674be))

### [0.0.11](https://gitlab.com/tjmonsi/lit-router/compare/v0.0.10...v0.0.11) (2020-08-30)


### Bug Fixes

* add typings ([421072d](https://gitlab.com/tjmonsi/lit-router/commit/421072d99de3b2ed9c33d276111c5a41510772da))

### [0.0.10](https://gitlab.com/tjmonsi/lit-router/compare/v0.0.9...v0.0.10) (2020-08-30)


### Bug Fixes

* fix on saving properties to appendedElement ([000ada4](https://gitlab.com/tjmonsi/lit-router/commit/000ada4bbc716a12ba6a75dbde10aceebdebd642))

### [0.0.9](https://gitlab.com/tjmonsi/lit-router/compare/v0.0.8...v0.0.9) (2020-08-30)


### Features

* add set routes and change route def ([cc27a3c](https://gitlab.com/tjmonsi/lit-router/commit/cc27a3cb343764473deaadf4b4e13ae888689e97))

### [0.0.8](https://gitlab.com/tjmonsi/lit-router/compare/v0.0.7...v0.0.8) (2020-08-30)

### [0.0.7](https://gitlab.com/tjmonsi/lit-router/compare/v0.0.6...v0.0.7) (2020-08-30)


### Bug Fixes

* fix to auto define element when loading getRouter ([87c2f8f](https://gitlab.com/tjmonsi/lit-router/commit/87c2f8f42e95139c8d0861ff1883697bc78aebc2))

### [0.0.6](https://gitlab.com/tjmonsi/lit-router/compare/v0.0.5...v0.0.6) (2020-08-30)


### Bug Fixes

* fix to auto define element when loading getRouter ([427dbe8](https://gitlab.com/tjmonsi/lit-router/commit/427dbe8f00bf1fa178db53aed1016878d61db2b8))

### [0.0.5](https://gitlab.com/tjmonsi/lit-router/compare/v0.0.4...v0.0.5) (2020-08-30)


### Bug Fixes

* fix deps on lit-location ([56060c4](https://gitlab.com/tjmonsi/lit-router/commit/56060c462ccf158675a7c98c577a51e635280bdd))

### [0.0.4](https://gitlab.com/tjmonsi/lit-router/compare/v0.0.3...v0.0.4) (2020-08-30)


### Bug Fixes

* fix variables to put it on constructor ([ae82a24](https://gitlab.com/tjmonsi/lit-router/commit/ae82a2463c9d57249351786dd00973a5dd1a0dd2))

### [0.0.3](https://gitlab.com/tjmonsi/lit-router/compare/v0.0.2...v0.0.3) (2020-08-30)


### Bug Fixes

* fix deps on lit-location and lit-query ([cf76dc2](https://gitlab.com/tjmonsi/lit-router/commit/cf76dc267aa311b017d76c47797fd2b8b618af22))
* fix deps on lit-location and lit-query ([876f378](https://gitlab.com/tjmonsi/lit-router/commit/876f37838124a445849d70f1f380a1677f64a126))

### [0.0.2](https://gitlab.com/tjmonsi/lit-router/compare/v0.0.1...v0.0.2) (2020-08-30)


### Bug Fixes

* fix for parsing class decorators fail ([4a439df](https://gitlab.com/tjmonsi/lit-router/commit/4a439df06cf21863696c067f9ac146431df3e594))

### 0.0.1 (2020-08-30)


### Features

* add lit-router ([9ef2c8a](https://gitlab.com/tjmonsi/lit-router/commit/9ef2c8a968ec64379758a679263d68fa8d382200))
