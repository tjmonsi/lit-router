import { getRouter } from './get-router';

/**
 *
 * @param {function} routes
 * @param {string} selector?
 * @param {HTMLElement} el?
 * @param {boolean} force?
 */
export const addRouteChangeErrorHandler = (
  fn,
  selector = 'lit-router',
  el = document,
  force = false) => {
  const router = getRouter(selector, el, force);
  if (router) {
    router.addEventListener('route-change-error', fn);
  }
}

/**
 *
 * @param {function} routes
 * @param {string} selector?
 * @param {HTMLElement} el?
 * @param {boolean} force?
 */
export const removeRouteChangeErrorHandler = (
  fn,
  selector = 'lit-router',
  el = document,
  force = false) => {
  const router = getRouter(selector, el, force);
  if (router) {
    router.removeEventListener('route-change-error', fn);
  }
}
