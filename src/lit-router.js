import {
  LitElement, html
} from 'lit-element';

import pathToRegexp from './path-to-regexp';
import { changeUrl } from './change-url';

import '@tjmonsi/lit-location/lit-location';
import '@tjmonsi/lit-query/lit-query';

class LitRouter extends LitElement {
  static get properties () {
    return {
      fallbackRoute: {
        type: String,
        attribute: 'fallback-route'
      },
      currentRoute: {
        type: String
      },
      path: {
        type: String
      },
      hash: {
        type: String
      },
      query: {
        type: String
      },
      routes: {
        type: Object
      }
    };
  }

  /**
   * @param {string} newValue
   */
  set path (newValue) {
    const oldValue = this.path;
    if (this._routeInitialized) {
      this._pathChanged(newValue);
    }
    this.requestUpdate('path', oldValue);
  }

  render () {
    return html`
      <lit-location
        @path-change="${this._boundLitePathChanged}"
        @hash-change="${this._boundLiteHashChanged}"
        @query-change="${this._boundLiteQueryChanged}">
      </lit-location>
      <lit-query
        .query=${this.query}
        @query-object-change="${this._boundLiteQueryObjectChanged}">
      </lit-query>
    `;
  }

  constructor () {
    super();

    // initialize variables
    /** @type {string} */
    this.container = '#main-container';

    /** @type {HTMLElement} */
    this.body = document;

    /** @type {string?} */
    this.componentName = null;

    this._boundLitePathChanged = this._litePathChanged.bind(this);
    this._boundLiteHashChanged = this._liteHashChanged.bind(this);
    this._boundLiteQueryChanged = this._liteQueryChanged.bind(this);
    this._boundLiteQueryObjectChanged = this._liteQueryObjectChanged.bind(this);

    /** @type {*} */
    this.routes = {};
    this._routeInitialized = false;
  }

  connectedCallback () {
    super.connectedCallback();

    if (!this.fallbackRoute) this.fallbackRoute = 'not-found';

    this._routeInitialized = true;
    if (this.path) this._pathChanged(this.path);
  }

  /**
   *
   * @param {string} url
   */
  changeUrl (url) {
    changeUrl(url);
  }

  /**
   *
   * @param {*} param
   */
  _litePathChanged ({ detail: path }) {
    if (this.path !== path) {
      this.path = path;
      if (this.appendedElement) {
        this.appendedElement.path = path;
      }
    }
  }

  /**
   *
   * @param {*} param
   */
  _liteHashChanged ({ detail: hash }) {
    if (this.hash !== hash) {
      this.hash = hash;
      if (this.appendedElement) {
        this.appendedElement.path = hash;
      }
    }
  }

  /**
   *
   * @param {*} param
   */
  _liteQueryChanged ({ detail: query }) {
    if (this.query !== query) {
      this.query = query;
      if (this.appendedElement) {
        this.appendedElement.query = query;
      }
    }
  }

  /**
   *
   * @param {*} param
   */
  _liteQueryObjectChanged ({ detail: queryObject }) {
    this.queryObject = queryObject;
    if (this.appendedElement) {
      this.appendedElement.queryObject = queryObject;
    }
  }

  /**
   *
   * @param {*} path
   */
  _pathChanged (path = '/') {
    let exec = null;
    let re = null;
    /**
     * @type {Array<*>}
     */
    let keys = [];

    for (const route of Object.keys(this.routes)) {
      keys = [];
      re = pathToRegexp(route, keys);
      exec = re.exec(path);

      if (exec) return this._routeMatched(route, exec, keys);
    }

    return this._routeMatched(this.fallbackRoute, [], []);
  }

  /**
   *
   * @param {*} route
   * @param {*} exec
   * @param {Array<*>} keys
   */
  async _routeMatched (currentRoute, exec, keys) {
    /**
     * @type {Object<string, *>}
     */
    const paramObject = {};
    for (let i = 0; i < keys.length; i++) {
      const key = keys[i];
      const { name } = key;
      paramObject[name] = exec[i + 1] || null;
    }

    this.paramObject = paramObject;
    this.currentRoute = currentRoute;

    await this.updateComplete;

    this.dispatchEvent(new window.CustomEvent('param-object-change', {
      detail: paramObject
    }));
    this.dispatchEvent(new window.CustomEvent('current-route-change', {
      detail: currentRoute
    }));

    if (!this.routes || !this.routes[currentRoute]) {
      console.error(`No route found for ${currentRoute}`);
    }

    const {
      preload,
      loader,
      fallback,
      component,
      properties,
      attributes
    } = this.routes[currentRoute] || {};

    if (preload) {
      if (typeof preload === 'function') {
        try {
          const result = await preload();
          if (typeof result === 'string' || result === false) {
            if (typeof result === 'string') {
              this.dispatchEvent(new window.CustomEvent('route-change-error', {
                detail: new Error(result)
              }));
            }

            changeUrl(typeof fallback === 'string' ? fallback : '/');
            return; // breaks the change
          }
        } catch (error) {
          console.error(error);
          this.dispatchEvent(new window.CustomEvent('route-change-error', {
            detail: error
          }));
          changeUrl(typeof fallback === 'string' ? fallback : '/');
          return;
        }
      } else if (preload instanceof Array) {
        const promises = [];
        try {
          for (const fn of preload) {
            if (typeof fn === 'function') {
              promises.push(fn());
            }
          }

          const results = await Promise.all(promises);

          for (const result of results) {
            if (typeof result === 'string' || result === false) {
              if (typeof result === 'string') {
                this.dispatchEvent(new window.CustomEvent('route-change-error', {
                  detail: new Error(result)
                }));
              }

              changeUrl(typeof fallback === 'string' ? fallback : '/');
              return; // breaks the change
            }
          }
        } catch (error) {
          console.error(error);
          this.dispatchEvent(new window.CustomEvent('route-change-error', {
            detail: error
          }));
          changeUrl(typeof fallback === 'string' ? fallback : '/');
          return;
        }
      }
    }

    if (loader && typeof loader === 'function') {
      await loader();
    }

    const main = this.body.querySelector(this.container);

    if (main) {
      if (typeof component === 'string' && this.body) {
        let el;
        if (this.componentName &&
          this.componentName === component) {
          // this means the component will not be changed;
          el = main.querySelector(component);
        } else {
          // remove all elements in main
          while (main.firstChild) main.removeChild(main.firstChild);
          el = /** @type {*} */(document
            .createElement(component));
        }

        if (!this.componentName ||
          this.componentName !== component) {
          main.appendChild(el);
          this.appendedElement = el;
        }

        this.componentName = component;
      } else if (typeof component === 'function') {
        const el = component();
        while (main.firstChild) main.removeChild(main.firstChild);
        main.appendChild(el);
        this.appendedElement = el;
      }

      // put the param object to the system
      if (properties) {
        for (const key in properties) {
          this.appendedElement[key] = properties[key];
        }
      }

      if (attributes) {
        for (const key in attributes) {
          this.appendedElement.setAttribute(key,
            typeof attributes[key] === 'function'
              ? attributes[key]()
              : attributes[key]);
        }
      }

      this.appendedElement.path = this.path;
      this.appendedElement.hash = this.hash;
      this.appendedElement.query = this.query;
      this.appendedElement.paramObject = this.paramObject;
      this.appendedElement.queryObject = this.queryObject;
    }
  }
}

export { LitRouter };
