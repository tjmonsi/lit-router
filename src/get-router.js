import '../lit-router';

let router = '';

export const getRouter = (
  selector = 'lit-router',
  el = document,
  force = false) => {
  if (!router || force) {
    router = el.querySelector(selector);
  }

  return router;
};
