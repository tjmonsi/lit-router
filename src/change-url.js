/**
 *
 * @param {string} url
 */
export const changeUrl = url => {
  window.history.pushState({}, '', url);
  window.dispatchEvent(new window.CustomEvent('location-change'));
};
