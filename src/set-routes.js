import { getRouter } from './get-router';

/**
 *
 * @param {*} routes
 * @param {string} selector
 * @param {HTMLElement} el
 * @param {boolean} force
 */
export const setRoutes = (
  routes,
  selector = 'lit-router',
  el = document,
  force = false) => {
  const router = getRouter();
  if (router) {
    router.routes = routes;
  }
}
