```js script
import { html } from '@open-wc/demoing-storybook';
import '../lit-router.js';

export default {
  title: 'LitRouter',
  component: 'lit-router',
  options: { selectedPanel: "storybookjs/knobs/panel" },
};
```

# LitRouter

A component for...

## Features:

- a
- b
- ...

## How to use

### Installation

```bash
yarn add lit-router
```

```js
import 'lit-router/lit-router.js';
```

```js preview-story
export const Simple = () => html`
  <lit-router></lit-router>
`;
```

## Variations

###### Custom Title

```js preview-story
export const CustomTitle = () => html`
  <lit-router title="Hello World"></lit-router>
`;
```
