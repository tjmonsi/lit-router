import { LitElement } from 'lit-element';

declare class LitRouter extends LitElement {
  path: String
  hash: String
  query: String
  fallbackRoute: String
  currentRoute: String
  container: String
  queryObject: {
    [key: string]: any
  }
  paramObject: {
    [key: string]: any
  }
  changeUrl: typeof changeUrl
}

class RouteChangeErrorEvent extends CustomEvent {
  detail: Error
}

declare function changeUrl (url: string)
declare function ComponentGenerator (): LitElement | HTMLElement
declare function LazyLoader (): Promise<any>
declare function AttributeFunction (): number | string | boolean
declare function PreLoader (): Promise<boolean | string | null | void>
declare function RouteChangeErrorHandler (
  event: RouteChangeErrorEvent
)

declare function getRouter (
  selector?: string,
  el?: HTMLElement | HTMLDocument,
  force?: boolean
): LitRouter

declare function addRouteChangeErrorHandler (
  fn: typeof RouteChangeErrorHandler,
  selector?: string,
  el?: HTMLElement | HTMLDocument,
  force?: boolean
)

declare function removeRouteChangeErrorHandler (
  fn: typeof RouteChangeErrorHandler,
  selector?: string,
  el?: HTMLElement | HTMLDocument,
  force?: boolean
)

declare function setRoutes (
  routes: {
    [key: string]: {
      component: string | typeof ComponentGenerator,
      loader: typeof LazyLoader,
      preload?: typeof PreLoader | [typeof PreLoader],
      fallback?: string,
      properties?: {
        [key: string]: any
      },
      attributes?: {
        [key: string]: number | string | boolean | typeof AttributeFunction
      }
    }
  },
  selector?: string,
  el?: HTMLElement | HTMLDocument,
  force?: boolean
)

export {
  LitRouter,
  getRouter,
  setRoutes,
  changeUrl,
  addRouteChangeErrorHandler,
  removeRouteChangeErrorHandler,
  LazyLoader,
  ComponentGenerator,
  AttributeFunction }
